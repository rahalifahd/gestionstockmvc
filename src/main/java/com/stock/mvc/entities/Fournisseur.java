package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Fournisseur implements Serializable{
	@Id
	@GeneratedValue
	private Long idFournisseur;
	private String nomFourn;
	private String prenomFourn;
	private String adresseFourn;
	private String photoFourn;
	private String mailFourn;
	@OneToMany(mappedBy="fournisseur")
	private List<CommandeFournisseur> commandeFournisseurs;
	
	public Long getIdFournisseur() {
		return idFournisseur;
	}

	public void setIdFournisseur(Long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}

	public String getNomFourn() {
		return nomFourn;
	}

	public void setNomFourn(String nomFourn) {
		this.nomFourn = nomFourn;
	}

	public String getPrenomFourn() {
		return prenomFourn;
	}

	public void setPrenomFourn(String prenomFourn) {
		this.prenomFourn = prenomFourn;
	}

	public String getAdresseFourn() {
		return adresseFourn;
	}

	public void setAdresseFourn(String adresseFourn) {
		this.adresseFourn = adresseFourn;
	}

	public String getPhotoFourn() {
		return photoFourn;
	}

	public void setPhotoFourn(String photoFourn) {
		this.photoFourn = photoFourn;
	}

	public String getMailFourn() {
		return mailFourn;
	}

	public void setMailFourn(String mailFourn) {
		this.mailFourn = mailFourn;
	}
	
	
	
	public List<CommandeFournisseur> getCommandeFournisseurs() {
		return commandeFournisseurs;
	}

	public void setCommandeFournisseurs(List<CommandeFournisseur> commandeFournisseurs) {
		this.commandeFournisseurs = commandeFournisseurs;
	}

	public Fournisseur() {
		// TODO Auto-generated constructor stub
	}
}
