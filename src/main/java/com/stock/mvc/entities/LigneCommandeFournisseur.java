package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeFournisseur  implements Serializable{
	@Id
	@GeneratedValue
	private Long idLigneCmdF;
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name="CmdF")
	private CommandeFournisseur commandeFournisseur;
	
	public Long getIdLigneCmdF() {
		return idLigneCmdF;
	}

	public void setIdLigneCmdF(Long idLigneCmdF) {
		this.idLigneCmdF = idLigneCmdF;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
	
	

}
