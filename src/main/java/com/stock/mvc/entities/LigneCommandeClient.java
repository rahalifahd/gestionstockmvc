package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class LigneCommandeClient implements Serializable{
	@Id
	@GeneratedValue
	private Long idLigneCmdC;
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name="idCmdC")
	private CommandeClient commandeClient;

	public Long getIdLigneCmdC() {
		return idLigneCmdC;
	}

	public void setIdLigneCmdC(Long idLigneCmdC) {
		this.idLigneCmdC = idLigneCmdC;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}
	
	
     
	
}
