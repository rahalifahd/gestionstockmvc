package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Utilisateur implements Serializable {
	@Id
	@GeneratedValue
	private Long idUtilis;
	private String nomUtilis;
	private String prenomUtilis;
	private String mail;
	private String motDePasse;
	private String photo;
	

	public Long getIdUtilis() {
		return idUtilis;
	}

	public void setIdUtilis(Long idUtilis) {
		this.idUtilis = idUtilis;
	}

	public String getNomUtilis() {
		return nomUtilis;
	}

	public void setNomUtilis(String nomUtilis) {
		this.nomUtilis = nomUtilis;
	}

	public String getPrenomUtilis() {
		return prenomUtilis;
	}

	public void setPrenomUtilis(String prenomUtilis) {
		this.prenomUtilis = prenomUtilis;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	

}
