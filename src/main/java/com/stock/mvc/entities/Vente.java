package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Vente implements Serializable {
	@Id
	@GeneratedValue
	private Long idVente;
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;
	@OneToMany(mappedBy="vente")
	private List<LigneVente> ligneVentes;

	public Long getIdVente() {
		return idVente;
	}

	public void setIdVente(Long idVente) {
		this.idVente = idVente;
	}

	
	
}
